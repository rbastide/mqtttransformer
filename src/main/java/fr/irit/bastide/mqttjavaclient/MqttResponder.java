package fr.irit.bastide.mqttjavaclient;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 * Un programme qui s'abonne à un topic mqtt et publie sur un autre topic quand il reçoit un message
 * @author rbastide
 */

public class MqttResponder {

	// Le broker auquel on se connecte
	//static final String BROKER = "tcp://test.mosquitto.org:1883";
	static final String BROKER = "tcp://broker.mqttdashboard.com:1883";
	// Le topic sur lequel on publie
	static final String PUBLISH_TOPIC = "bastide/grove/otherTopic";
	// Le topic auquel on s'abonne
	static final String SUBSCRIBE_TOPIC = "bastide/grove/potentiometer";
	// Le contenu du message envoyé
	static final String CONTENT = "Message from MqttPublishSample";
	// La qualité de service
	static final int QOS = 2;

	//@see : https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttClient.html
	final MqttClient sampleClient;

	public static void main(String[] args) throws Exception {
		System.out.printf("Ce client s'abonne au topic %s et répond dans le topic %s sur le broker %s %n",
			SUBSCRIBE_TOPIC, PUBLISH_TOPIC, BROKER);
		
		MqttResponder transformer = new MqttResponder();
		transformer.connect();
		System.out.println("Connected - Hit enter to terminate");

		// attendre la frappe d’une touche dans la console
		System.in.read();

		transformer.disconnect();
		System.out.println("Disconnected");
		//System.exit(0);
	}

	public MqttResponder() throws MqttException {
		sampleClient = new MqttClient(BROKER, MqttClient.generateClientId());
	}

	public void disconnect() throws MqttException {
		sampleClient.disconnect();
		sampleClient.close();
	}

	public void connect() throws MqttException {
		// Les options de connection
		MqttConnectOptions connOpts = new MqttConnectOptions();
		connOpts.setCleanSession(true);
		connOpts.setAutomaticReconnect(true);

		// Le callback pour réagir aux événements du client
		sampleClient.setCallback(new MqttCallback() {
			@Override
			public void connectionLost(Throwable t) {
				System.err.printf("Connection lost: %s %n", t.getMessage());
			}

			@Override
			public void messageArrived(String string, MqttMessage mm) throws Exception {
				//System.out.printf("Message %s arrived %n", new String(mm.getPayload()));
			}

			@Override
			public void deliveryComplete(IMqttDeliveryToken imdt) {
				System.out.println("Message published");
			}
		});

		System.out.printf("Client %s is connecting to broker: %s %n", sampleClient.getClientId(), BROKER);
		sampleClient.connect(connOpts);

		// On s'abonne
		sampleClient.subscribe(SUBSCRIBE_TOPIC,
			// Le callback pour répondre aux messages reçus
			new IMqttMessageListener() {
			@Override
			public void messageArrived(String string, MqttMessage mm) throws Exception {

				String contents = String.format(
					"%s publishes '%s' in response to %s",
					sampleClient.getClientId(),
					CONTENT,
					new String(mm.getPayload())
				);

				System.out.println(contents);
				MqttMessage message = new MqttMessage(contents.getBytes());
				message.setQos(QOS);
				sampleClient.publish(PUBLISH_TOPIC, message);
			}
		});
		System.out.printf("%s has subscribed to %s %n", sampleClient.getClientId(), SUBSCRIBE_TOPIC);
	}
}
