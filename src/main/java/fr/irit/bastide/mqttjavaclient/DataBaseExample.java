package fr.irit.bastide.mqttjavaclient;
/**
 * Exemple d'utilisation de la base de données Patients simulée
 * @author rbastide
 */

public class DataBaseExample {
	public static void main(String[] args) {
		Patient p1 = new Patient("Remi Bastide");
		p1.setScore(Braden.ACTIVITY, 2)
		  .setScore(Braden.FRICTION, 2);
		Patient p2 = new Patient("Elyes Lamine");
		p2.setScore(Braden.NUTRITION, 2)
		  .setScore(Braden.MOISTURE, 3);

		PatientDatabase database = new PatientDatabase();
		
		database.addPatient(1, p1)
			.addPatient(2, p2);
		
		System.out.println(database.getPatient(2));
	}
	
}
