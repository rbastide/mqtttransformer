package fr.irit.bastide.mqttjavaclient;
import java.util.HashMap;
import java.util.Map;

/**
 * Simulation d'une base de données patient
 * @author rbastide
 */
public class PatientDatabase {
	final Map<Integer, Patient> patients = new HashMap<>();
	
	public PatientDatabase addPatient(int code, Patient p) {
		patients.put(code, p);
		return this;
	}

	public Patient getPatient(int code) {
		return patients.get(code);
	}
}
