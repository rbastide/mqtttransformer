package fr.irit.bastide.mqttjavaclient;
/**
 * Les critères de l'échelle de Braden
 * @author rbastide
 */
public enum Braden {
	SENSITIVITY,
	NUTRITION,
	MOBILITY,
	ACTIVITY,
	MOISTURE,
	FRICTION
}
