package fr.irit.bastide.mqttjavaclient;

import java.util.HashMap;
import java.util.Map;

/**
 * Un patient dans la base de données, avec son score de Braden
 * @author rbastide
 */

public class Patient {
	private final Map<Braden, Integer> scores = new HashMap<>();
	private final String name;
	

	public Patient(String name) {
		this.name = name;
		for (Braden criterion : Braden.values()) {
			// On initialise les scores de Braden
			setScore(criterion, 2);
		}
	}
	
	public Patient setScore(Braden criterion, int newScore) {
		scores.put(criterion, Math.abs(newScore) % 5);
		return this;
	}
	
	public int getScore(Braden criterion) {
		return scores.get(criterion);
	}

	public String getName() {
		return name;
	}

	public int getBradenScore() {
		int result = 0;
		for (Braden criterion : Braden.values()) {
			int value = scores.get(criterion);
			result += value;
		}
		return result;
	}

	@Override
	public String toString() {
		return "Patient{name=" + name + ", score=" + getBradenScore() + " scores=" + scores + '}';
	}
}
